# SYNOPSIS

Chemaxon app health check

# INSTALL

    $ sparrow plg install chemaxon

# USAGE

    $ sparrow plg run chemaxon

# Author

Alexey Melezhik



